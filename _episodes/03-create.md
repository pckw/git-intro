---
title: Creating a Repository
teaching: 10
exercises: 3
questions:
- "Where does Git store information?"
objectives:
- "Create a local Git repository."
keypoints:
- "`git init` initializes a repository."
- "Read the output of your git commands!"
---

## Your first project

Once Git is configured, we can start using it.

Let's create a directory for our work and then move into that directory:

~~~
$ mkdir planets
$ cd planets
~~~
{: .bash}

Then we tell Git to make `planets` a [repository]({{ page.root }}/reference/#repository)—a place where
Git can store versions of our files:

~~~
$ git init
~~~
{: .bash}

If we use `ls` to show the directory's contents,
it appears that nothing has changed:

~~~
$ ls
~~~
{: .bash}

But if we add the `-a` flag to show everything,
we can see that Git has created a hidden directory within `planets` called `.git`:

~~~
$ ls -a
~~~
{: .bash}

~~~
.	..	.git
~~~
{: .output}

Git stores information about the project in this special sub-directory. If we
ever delete it, we will lose the project's history.

## Status of the project

We can check that everything is set up correctly by asking Git to tell us the
status of our project:

~~~
$ git status
~~~
{: .bash}

~~~
# On branch master
#
# Initial commit
#
nothing to commit (create/copy files and use "git add" to track)
~~~
{: .output}

As we'll see much more clearly later, the output of Git is a valuable source of
information:

- We're on branch `master`.
- We're on the Initial commit
- There's nothing to be committed.
