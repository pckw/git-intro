---
title: Remotes in Gitlab
teaching: 10
exercises: 0
questions:
- "How do I share my changes with others?"
- "How do I use Gitlab?"
objectives:
- "Explain what remote repositories are and why they are useful."
- "Push to or pull from a remote repository."
keypoints:
- "Even before this episode, you had a very powerful version control system."
- "A local Git repository can be connected to one or more remote repositories."
- "Use the HTTPS protocol to connect to remote repositories if you did not set up SSH yet."
- "`git push` copies changes from a local repository to a remote repository."
- "`git pull` copies changes from a remote repository to a local repository."
---

## Where to host repositories?

Version control really comes into its own when we begin to collaborate with
other people.  We already have most of the machinery we need to do this; the
only thing missing is to copy changes from one repository to another.

Systems like Git allow us to move work between any two repositories.  In
practice, though, it's easiest to use one copy as a central hub, and to keep it
on the web rather than on someone's laptop.  Most programmers use hosting
services like [GitHub](http://github.com), [BitBucket](http://bitbucket.org) or
[GitLab](http://gitlab.com/) to hold those master copies.

## Create a project / repository

Let's start by sharing the changes we've made to our current project with the
"world".  Log in to Geomar's Gitlab server <https://git.geomar.de>, then click
on the icon in the top right corner to create a new project called `planets`:

![Creating a Project on Gitlab (Step 1)](../fig/gitlab_01_initial_view.png)

Name your repository "planets" and then click "Create Project":

![Creating a Project on Gitlab (Step 2)](../fig/gitlab_02_create_project.png)

As soon as the project is created, Gitlab displays a page with a URL and some
information on how to configure your local repository:

![Creating a Project on Gitlab (Step 3)](../fig/gitlab_03_created_project.png)


> ## HTTPS vs. SSH
>
> HTTPS does not require additional configuration.  It is, however, preferrable
> to set up SSH access which makes it easier to work without having to re-type
> your login data on every commit / push.  To setup SSH access, read one of the
> tutorials from
> [GitHub](https://help.github.com/articles/generating-ssh-keys),
> [Atlassian/BitBucket](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)
> and [GitLab](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/)
> (this one has a screencast).
 {: .callout}

Make sure to use the URL for your repository rather than the dummy-one provided
here.

## Check which remotes are known

We can check that the command has worked by running `git remote -v` in the
local repository:

~~~
$ git remote -v
~~~
{: .bash}
~~~
origin   git@git.geomar.de:<your-username>/planets.git (push)
origin   git@git.geomar.de:<your-username>/planets.git (fetch)
~~~
{: .output}

The name `origin` is a local nickname for your remote repository: we could use
something else if we wanted to, but `origin` is by far the most common choice.

## Push changes to the remote

Once the nickname `origin` is set up, this command will push the changes from
our local repository to the repository on GitHub:

~~~
$ git push -u origin master
~~~
{: .bash}
~~~
Counting objects: 9, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 821 bytes, done.
Total 9 (delta 2), reused 0 (delta 0)
To git@git.geomar.de:<your-username>/planets.git
 * [new branch]      master -> master
Branch master set up to track remote branch master from origin.
~~~
{: .output}

Our local and remote repositories are now in this state:

![Gitlab Repository After First Push](../fig/gitlab-repo-after-first-push.svg)

> ## The '-u' Flag
>
> We used `git push -u origin master`.  The `-u`-option associates the local
> branch `master` with `origin/master` which is our nickname for `master` in
> the remote repository.  This way, we can later just use `git push` and Git
> knows where to push.
>
 {: .callout}

## Pulling changes from the remote

We can pull changes from the remote repository to the local one as well:

~~~
$ git pull
~~~
{: .bash}
~~~
From git@git.geomar.de:<your-username>/planets.git
 * branch            master     -> FETCH_HEAD
Already up-to-date.
~~~
{: .output}

Pulling has no effect in this case because the two repositories are already
synchronized.  If someone else had pushed some changes to the repository on
GitLab, though, this command would download them to our local repository.

---