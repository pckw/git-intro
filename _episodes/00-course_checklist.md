---
title: Before we get started
teaching: 5
exercises: 0
objectives:
- "Login to a workstation or set up your own laptop"
- "Make sure git is installed."
- Make sure you are connected to the internet.
---

## Have a WiFi connection?

## Have Git on your computer?

On Linux, try `git --version` on the command line.  

On Mac, try `git --version` in a terminal and  

On Windows, installed "Git for Windows" from:  
<https://www.atlassian.com/git/tutorials/install-git#windows>

## Course material

You can access the course material on: [https://pckw.gitlab.io/git-intro/](https://pckw.gitlab.io/git-intro/).
