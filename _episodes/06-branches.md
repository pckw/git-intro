---
title: Branches
teaching: 15
exercises: 0
questions:
- "How can I work on a feature without spoiling my main code?"
objectives:
- "Create a branch, commit there, and merge it back."
- "Understand the flow of information in a simple workflow using branch, commit, merge."
keypoints:
- "`git brach <branch name>` creates a branch."
- "`git checkout <branch name>` switches to this new branch."
- "`git checkout master` switches back to the master branch."
- "`git merge <branch name>` merges branch `<branch name>` into the current branch."
- "`git branch -d <branch name>` deletes the branch."
---


Note that the ideas described in this episode will be the main foundation for
collaborative work.  Note also that this episode heavily leans
on [Chapter 3][progit2-chapter-03] of the [Pro Git book][progit2-book].

[progit2-chapter-03]: https://www.git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell
[progit2-book]: https://www.git-scm.com/book/en/v2


## A typical work flow

We often (most of the time?) do not work in a linear way where we advance by
consecutive steps, never go back, or are stuck in dead ends.  Our work flows
often include concurrent changes to different parts of our work.  We, e.g.,
work on an introductory section of a chapter while in a different window adding
things to anoter part of the same document.

The basic building block of this workflow looks like this:

![Branch your project, modify, and merge.](../fig/branch_01.svg)


## Outline of what is following

We will deviate from our planets project for a while and imagine the following
situation:

- We want to add a some experimental features to a project, which has only one branch.

- To leave the main project unchanged until we are sure that our new idea works out we create a branch called `testing`, develop our  solution there, and only merge our changes back into the main branch when we are done.

- Before `testing` is merged into the master branch, we have another idea for our project which we inlcude in our master branch.

- In the end, we will merge the branch `testing` back to `master` which by
  that time has advanced from where we started when creating `testing`.


## Initial state

We start with a linear history and a branch `master` and create another branch called `testing`.  
GIT keeps a speacial pointer called `HEAD` to indicate the branch we are currently on.

~~~
$ git branch testing
~~~
{: .bash}

![Master at C2](../fig/head-to-master.png)


## Start working on branch `testubg`

Before we make any changes, switch to the new branch.  
Note that it points to the same commit as `master`:

~~~
$ git checkout testing
~~~
{: .bash}

![Master and iss53 at C2](../fig/head-to-testing.png)

We then work on branch `testing`, change something, commit it, and hence advance
the branch `testing` to a new commit.

~~~
$ nano some_file.py
$ git add some_file.py
$ git commit -m "Test my new idea."
~~~  
{: .bash}

![Master at C2 and iss53 at C3](../fig/progit2_advance-testing.png)


## Switch back master branch

Now, we go back to `master`, edit something and commit
it:

~~~
$ git checkout master
$ nano some_other_file.py
$ git add some_other_file.py
$ git commit -m "Adress some other problem"
~~~
{: .bash}

This advances the branch `master` to a new commit:

![Master at C2, iss53 at C3, hotfix at C4](../fig/advance-master.png)


## Merge `testing` to `master`

After some testing, we are confident that we want our new idea.  We merge it into `master`:

~~~
$ git merge testing
~~~
{: .bash}

A new commit is created that includes the changes we made on the testing branch. This leaves us with:

![Master and hotfix at C4, iss53 at C3](../fig/progit2_merge-testing.png)


## Delete `testing`

The branch `testing` is not needed anymore.  We delete it:

~~~
$ git branch -d testing
~~~
{: .bash}

![Master and hotfix at C4, iss53 at C3](../fig/progit2_delete-testing.png)

Note that this **does not delete any commits** we just remove a pointer called `testing`.

