---
title: Hands-On Session
teaching: 0
exercises: 30
questions:
- "How do I initialize a repository?"
- "How do I add and commit changes?"
- "How can I explore the history of my repository?"
- "How can I undo changes?"
objectives:
- "Repeat the most common steps you encounter when working with Git."
keypoints:
- "Get (at least mildly) comfortable with basic forward steps with Git."
---

> ## Create an empty repository
>
> Create a directory `planets` to work in.  Initialize a Git repository there.
>
> > ## Solution
> > 
> > ~~~
> > $ mkdir planets
> > $ cd planets
> > $ git init
> > ~~~
> > {: .bash}
> >
>  {: .solution}
   {: .challenge}


> ## Create two files
>
> Within `planets/`, create two files `mars.txt` and `jupiter.txt`, and fill
> them with content.  If you're lazy, [get inspiration
> here](https://en.wikipedia.org/wiki/Mars) or
> [here](https://en.wikipedia.org/wiki/Jupiter).
> 
> > ## Solution
> > 
> > ~~~
> > $ nano mars.txt
> > ~~~
> > {: .bash}
> > 
> > ~~~
> > $ nano jupiter.txt
> > ~~~
> > {: .bash}
> >
>  {: .solution}
   {: .challenge}


> ## Inspect your repository
>
> How can you check how your working directory differs from the last commit?
> 
> What else can you learn?
> 
> > ## Solution
> > 
> > ~~~
> > $ git status
> > ~~~
> > {: .bash}
> > 
> > `git status` also tells you where `HEAD` currently points.
> >
>  {: .solution}
   {: .challenge}


> ## Add and commit only one of the changed files
>
> How can you add and commit a file?  Do so with `mars.txt`.  Inspect the repository
> before and after committing.
> 
> > ## Solution
> > 
> > Add your file with:
> > 
> > ~~~
> > $ git add mars.txt
> > $ git commit -m "Initial commit of description of Mars."
> > ~~~
> > {: .bash}
> > 
> > Inspect the repo with:
> > 
> > ~~~
> > $ git status
> > ~~~
> > {: .bash}
> >
> > Then, commit the file and inspect the repo again:
> >
> > ~~~
> > $ git commit -m "Initial commit of description of Mars."
> > $ git status
> > ~~~
> > {: .bash}
> > 
>  {: .solution}
   {: .challenge}


> ## Add and commit the other file as well
>
> Now, add and commit the other file as well.  Inspect the repo as before.
> 
> > ## Solution
> > 
> > Add your file with:
> > 
> > ~~~
> > $ git add jupiter.txt
> > $ git commit -m "Initial commit of description of Jupiter."
> > $ git status
> > ~~~
> > {: .bash}
> > 
>  {: .solution}
   {: .challenge}


> ## Add a new set of changes: Change both files in one commit.
>
> Edit, add and commit both files.  Inspect the repo as before.
>
> _Excursion:_ Try a very long commit message (in the editor, of course) and see
> if you get any hints on how long the "short first line" can be.
> 
> > ## Solution
> > 
> > Add your file with:
> > 
> > ~~~
> > $ nano mars.txt
> > $ nano jupiter.txt
> > $ git add mars.txt jupiter.txt
> > $ git commit
> > $ git status
> > ~~~
> > {: .bash}
> > 
>  {: .solution}
   {: .challenge}


> ## Look at the log of your repo
>
> Look at the log of your repository.
> 
> _Excursion (again...):_ Play with detailed multi-line commit messages and see
> which are displayed in the different possibilities to display Git's log.
> 
> > ## Solution
> > 
> > Look at a very condensed form with:
> > 
> > ~~~
> > $ git log --oneline
> > ~~~
> > {: .bash}
> >
> > Or see more info with
> >
> > ~~~
> > $ git log
> > ~~~
> > {: .bash}
> >
>  {: .solution}
   {: .challenge}
   
> ## Undo changes
>
> Undo the second change you made to mars.txt.
>  
> > ## Solution
> > 
> > Go back one commit:
> > 
> > ~~~
> > $ git checkout master~1 mars.txt
> > $ git add mars.txt
> > $ git commit
> > ~~~
> > {: .bash}
> >
>  {: .solution}
   {: .challenge}
